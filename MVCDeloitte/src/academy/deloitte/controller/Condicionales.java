package academy.deloitte.controller;



/**
 * Clase usada para ralizar validaciones numericas con metodos estaticos.
 * 
 * @author haviles haviles@deloitteexternos.com
 * @version 1.0
 * 
 */
public class Condicionales {

	/**
	 * Metodo estatico usado para calcular el numero mayor entre dos numeros
	 * enteros.
	 * 
	 * @param x (Integer) Numero 1
	 * @param y (Integer) Numero 2
	 * @return El numero mayor entre los dos.
	 */
	public static Integer numeroMayor(Integer x, Integer y) {
		return (x > y) ? x : y;
	}

	/**
	 * Metodo estatico usado para calcular el numero menor entre dos numeros
	 * enteros.
	 * 
	 * @param x (Integer) Numero 1
	 * @param y (Integer) Numero 2
	 * @return El numero menor entre los dos.
	 */
	public static Integer numeroMenor(Integer x, Integer y) {
		return (x < y) ? x : y;
	}

	/**
	 * Metodo estatico usado para saber si un numero dado es par o no lo es.
	 * 
	 * @param x (Integer) Numero 1
	 * @param y (Integer) Numero 2
	 * @return True si el numero es par, False si no lo es.
	 */
	
	public static boolean esPar(Integer x) {
		return (x % 2 == 0) ? true : false;
	}
	
	/**
	 * Metodo estatico para saber el modulo de de algun numero (multiplo)
	 * retorna un booleano.
	 * @param num Integer. Numero a evaluar
	 * @param multiplo Integer. Numero Multiplo 
	 * @return retorna True si es multiplo, False si no lo es
	 */
	public static boolean moduloDe(Integer num, Integer mod) {
		if ((num % mod) == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Metodo estatico usado para saber que numero es menor entre 3 numeros dados por parametro
	 * retorna un integer.
	 * @param num1 Integer
	 * @param num2 Integer
	 * @param num3 Integer
	 * @return Retorna el numero menor de entre los 3.
	 */
	public static Integer numeroMenor(Integer num1, Integer num2, Integer num3) {

		if ((num1 < num2) && (num1 < num3)) {
			return num1;
		} else if ((num2 < num1) && (num2 < num3)) {
			return num2;
		} else {
			return num3;
		}
	}
}
