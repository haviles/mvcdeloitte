package academy.deloitte.models;


/**
 * Clase usada para almacenar la informacion de los articulos que estan
 * registrados en la maquina expendedora.
 * 
 * @author haviles
 *
 */

public class Articulo {
	private String idArticulo;
	private String nombreArticulo;
	private double costoArticulo;
	private Integer cantidad;
	private String atributo5;
	private String atributo6;
	private String atributo7;
	private String atributo8;
	private String atributo9;
	private String atributo10;
	private String atributo11;
	private String atributo12;
	private String atributo13;
	private String atributo14;
	private String atributo15;
	private String atributo16;
	private String atributo17;
	private String atributo18;
	private String atributo19;
	private String atributo20;
	
	/**
	 * Constructor principal
	 * 
	 * @param idArticulo Integer 
	 * @param nombreArticulo String
	 * @param costoArticulo Double
	 * @param cantidad Integer
	 * @param status Status
	 */
	
	
	public Articulo() {}
	public String getAtributo5() {
		return atributo5;
	}

	public Articulo(String idArticulo, String nombreArticulo, double costoArticulo, 
			Integer cantidad, String atributo5,String atributo6, String atributo7, 
			String atributo8, String atributo9, String atributo10,String atributo11, 
			String atributo12, String atributo13, String atributo14, String atributo15,
			String atributo16, String atributo17, String atributo18, String atributo19, 
			String atributo20) {
		
		super();
		this.idArticulo = idArticulo;
		this.nombreArticulo = nombreArticulo;
		this.costoArticulo = costoArticulo;
		this.cantidad = cantidad;
		this.atributo5 = atributo5;
		this.atributo6 = atributo6;
		this.atributo7 = atributo7;
		this.atributo8 = atributo8;
		this.atributo9 = atributo9;
		this.atributo10 = atributo10;
		this.atributo11 = atributo11;
		this.atributo12 = atributo12;
		this.atributo13 = atributo13;
		this.atributo14 = atributo14;
		this.atributo15 = atributo15;
		this.atributo16 = atributo16;
		this.atributo17 = atributo17;
		this.atributo18 = atributo18;
		this.atributo19 = atributo19;
		this.atributo20 = atributo20;
	}
	


	public void setAtributo5(String atributo5) {
		this.atributo5 = atributo5;
	}

	public String getAtributo6() {
		return atributo6;
	}

	public void setAtributo6(String atributo6) {
		this.atributo6 = atributo6;
	}

	public String getAtributo7() {
		return atributo7;
	}

	public void setAtributo7(String atributo7) {
		this.atributo7 = atributo7;
	}

	public String getAtributo8() {
		return atributo8;
	}

	public void setAtributo8(String atributo8) {
		this.atributo8 = atributo8;
	}

	public String getAtributo9() {
		return atributo9;
	}

	public void setAtributo9(String atributo9) {
		this.atributo9 = atributo9;
	}

	public String getAtributo10() {
		return atributo10;
	}

	public void setAtributo10(String atributo10) {
		this.atributo10 = atributo10;
	}

	public String getAtributo11() {
		return atributo11;
	}

	public void setAtributo11(String atributo11) {
		this.atributo11 = atributo11;
	}

	public String getAtributo12() {
		return atributo12;
	}

	public void setAtributo12(String atributo12) {
		this.atributo12 = atributo12;
	}

	public String getAtributo13() {
		return atributo13;
	}

	public void setAtributo13(String atributo13) {
		this.atributo13 = atributo13;
	}

	public String getAtributo14() {
		return atributo14;
	}

	public void setAtributo14(String atributo14) {
		this.atributo14 = atributo14;
	}

	public String getAtributo15() {
		return atributo15;
	}

	public void setAtributo15(String atributo15) {
		this.atributo15 = atributo15;
	}

	public String getAtributo16() {
		return atributo16;
	}

	public void setAtributo16(String atributo16) {
		this.atributo16 = atributo16;
	}

	public String getAtributo17() {
		return atributo17;
	}

	public void setAtributo17(String atributo17) {
		this.atributo17 = atributo17;
	}

	public String getAtributo18() {
		return atributo18;
	}

	public void setAtributo18(String atributo18) {
		this.atributo18 = atributo18;
	}

	public String getAtributo19() {
		return atributo19;
	}

	public void setAtributo19(String atributo19) {
		this.atributo19 = atributo19;
	}

	public String getAtributo20() {
		return atributo20;
	}

	public void setAtributo20(String atributo20) {
		this.atributo20 = atributo20;
	}

	public String getIdArticulo() {
		return idArticulo;
	}

	public void setIdArticulo(String idArticulo) {
		this.idArticulo = idArticulo;
	}

	public String getNombreArticulo() {
		return nombreArticulo;
	}

	public void setNombreArticulo(String nombreArticulo) {
		this.nombreArticulo = nombreArticulo;
	}

	public double getCostoArticulo() {
		return costoArticulo;
	}

	public void setCostoArticulo(double costoArticulo) {
		this.costoArticulo = costoArticulo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
}